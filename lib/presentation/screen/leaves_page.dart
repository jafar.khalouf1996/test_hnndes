import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_hnndes/business_logic/cubit/leavesCubit/leaves_cubit.dart';
import 'package:test_hnndes/data/api/leaves_api.dart';
import 'package:test_hnndes/business_logic/blocs/leaves_bloc/leaves_bloc.dart';
import 'package:test_hnndes/data/contants/clip_helper.dart';
import 'package:test_hnndes/data/contants/app_colors.dart';
import 'package:test_hnndes/data/contants/imagaeasset.dart';
import 'package:test_hnndes/data/contants/styles.dart';
import 'package:test_hnndes/main.dart';
import 'package:test_hnndes/presentation/screen/signin_page.dart';
import 'package:test_hnndes/presentation/widgets/loading_widget.dart';

class LeavesPage extends StatefulWidget {
  const LeavesPage({super.key});

  @override
  State<LeavesPage> createState() => _LeavesPageState();
}

class _LeavesPageState extends State<LeavesPage> {
  ScrollController sc = ScrollController();
  final LeavesCubit leavesBloc = LeavesCubit(leavesApi: LeavesApi());
  int userLeaves = 0;

  @override
  void initState() {
    sc.addListener(() {
      if (sc.position.pixels >= sc.position.maxScrollExtent) {
        if (leavesBloc.pageNumber <= leavesBloc.totalPages) {
          leavesBloc.getLeavesFetching();
        }
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => leavesBloc..getInitial(),
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              "My Leaves",
              style: TextStyle(
                  fontSize: 26.sp,
                  color: AppColors.blackColor,
                  fontWeight: FontWeight.bold),
            ),
            actions: [
              Icon(
                Icons.notifications_none_rounded,
                color: AppColors.blackColor,
              ),
              const SizedBox(
                width: 10,
              ),
              Icon(
                Icons.person_pin,
                color: AppColors.loadingColor,
              ),
              const SizedBox(
                width: 10,
              )
            ],
            centerTitle: true,
            backgroundColor: AppColors.whiteColor,
            elevation: 5,
          ),
          backgroundColor: AppColors.whiteColor,
          body: BlocConsumer<LeavesCubit, LeavesState>(
            listener: (context, state) {
              if (state is UnAuthorizedState) {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => const SigninPage(),
                ));
              }
            },
            builder: (context, state) {
              if (state is Loading) {
                return LoadingWidget(
                  height: 250.h,
                  width: 250.h,
                );
              } else if (state is LeavesFirstPageFailed) {
                return const Center(
                  child: Text("Error while loading"),
                );
              } else {
                return Column(
                  children: [
                    Expanded(
                      child: ListView.builder(
                          controller: sc,
                          itemCount: leavesBloc.leaves.length,
                          itemBuilder: (context, index) {
                            userLeaves = leavesBloc.leaveCount.annual! +
                                leavesBloc.leaveCount.death! +
                                leavesBloc.leaveCount.marriage! +
                                leavesBloc.leaveCount.maternity! +
                                leavesBloc.leaveCount.others! +
                                leavesBloc.leaveCount.sickness! +
                                leavesBloc.leaveCount.unpaid! +
                                leavesBloc.leaveCount.workAccident!;
                            return Column(
                              children: [
                                index == 0
                                    ? ClipPath(
                                        clipper: MyClip(),
                                        child: Container(
                                          height: 250.h,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 32.w, vertical: 16.h),
                                          decoration: BoxDecoration(
                                            color: Colors.purple[300],
                                          ),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                sharedPreferences!
                                                    .getString("emp_name")
                                                    .toString(),
                                                style: TextStyle(
                                                  color: AppColors.whiteColor,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 8.h,
                                              ),
                                              LinearProgressIndicator(
                                                value: userLeaves.toDouble() /
                                                    leavesBloc
                                                        .leaveCount.maxAnnual!,
                                                backgroundColor:
                                                    Colors.greenAccent,
                                                valueColor:
                                                    AlwaysStoppedAnimation<
                                                            Color>(
                                                        AppColors.redColor),
                                              ),
                                              SizedBox(
                                                height: 12.h,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    "$userLeaves Leaves",
                                                    style: TextStyle(
                                                        fontSize: 16.sp,
                                                        color: AppColors
                                                            .whiteColor),
                                                  ),
                                                  Text(
                                                    "${leavesBloc.leaveCount.maxAnnual} Leaves",
                                                    style: TextStyle(
                                                        fontSize: 16.sp,
                                                        color: AppColors
                                                            .whiteColor),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      )
                                    : Container(),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 16.w, vertical: 16.h),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 16.w, vertical: 12.h),
                                  decoration: BoxDecoration(
                                      color: AppColors.whiteColor,
                                      borderRadius: BorderRadius.circular(15.r),
                                      boxShadow: [
                                        BoxShadow(
                                          blurRadius: 5,
                                          color: AppColors.blackColor
                                              .withOpacity(0.5),
                                          spreadRadius: 2,
                                        )
                                      ]),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          SizedBox(
                                            width: 37.w,
                                            height: 37.w,
                                            child: Image.asset(
                                                AppImageAsset.icondata),
                                          ),
                                          SizedBox(
                                            width: 12.w,
                                          ),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Applied Duration",
                                                style: TextStyle(
                                                  fontSize: 22.sp,
                                                  color: AppColors.grey,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 8.h,
                                              ),
                                              Row(
                                                children: [
                                                  Text(
                                                    leavesBloc.leaves[index]
                                                                .absenceFrom !=
                                                            null
                                                        ? leavesBloc
                                                            .leaves[index]
                                                            .absenceFrom!
                                                            .substring(0, 10)
                                                        : "",
                                                    style: MyStyle
                                                        .primaryTextStyle(),
                                                  ),
                                                  Text(
                                                    "  to  ",
                                                    style: MyStyle
                                                        .primaryTextStyle(),
                                                  ),
                                                  Text(
                                                    leavesBloc.leaves[index]
                                                                .absenceTo !=
                                                            null
                                                        ? leavesBloc
                                                            .leaves[index]
                                                            .absenceTo!
                                                            .substring(0, 10)
                                                        : "",
                                                    style: MyStyle
                                                        .primaryTextStyle(),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 16.h,
                                      ),
                                      Row(
                                        children: [
                                          SizedBox(
                                            width: 37.w,
                                            height: 37.w,
                                            child: Image.asset(
                                                AppImageAsset.iconnote),
                                          ),
                                          SizedBox(
                                            width: 12.w,
                                          ),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Notes",
                                                style: TextStyle(
                                                  color: AppColors.grey,
                                                  fontSize: 22.sp,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 8.h,
                                              ),
                                              Text(
                                                leavesBloc.leaves[index]
                                                                .notes !=
                                                            null &&
                                                        leavesBloc.leaves[index]
                                                            .notes!.isNotEmpty
                                                    ? leavesBloc
                                                        .leaves[index].notes!
                                                    : "no notes",
                                                style:
                                                    MyStyle.primaryTextStyle(),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 16.h,
                                      ),
                                      Row(
                                        children: [
                                          SizedBox(
                                            width: 37.w,
                                            height: 37.w,
                                            child: Image.asset(
                                                AppImageAsset.iconstatus),
                                          ),
                                          SizedBox(
                                            width: 12.w,
                                          ),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Status",
                                                style: TextStyle(
                                                  color: AppColors.grey,
                                                  fontSize: 22.sp,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 8.h,
                                              ),
                                              Text(
                                                leavesBloc.leaves[index]
                                                        .statusName ??
                                                    "",
                                                style:
                                                    MyStyle.primaryTextStyle(),
                                              ),
                                            ],
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            );
                          }),
                    ),
                    BlocBuilder<LeavesCubit, LeavesState>(
                      builder: (context, state) {
                        if (state is LeavesLoading) {
                          return const LoadingWidget();
                        } else if (state is LeavesLoaded) {
                          return Container();
                        } else if (state is LeavesError) {
                          return GestureDetector(
                            onTap: () => leavesBloc.getLeavesFetching(),
                            child: SizedBox(
                              height: 100.h,
                              child: const Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "Error, click to try again",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500),
                                  ),
                                  Icon(Icons.refresh),
                                ],
                              ),
                            ),
                          );
                        }
                        return Container();
                      },
                    )
                  ],
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
