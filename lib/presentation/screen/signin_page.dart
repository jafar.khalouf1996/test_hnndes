import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_hnndes/business_logic/cubit/signin_cubit/signin_cubit.dart';
import 'package:test_hnndes/data/api/signin_api.dart';
import 'package:test_hnndes/business_logic/blocs/signin_bloc/signin_bloc.dart';
import 'package:test_hnndes/data/contants/app_colors.dart';
import 'package:test_hnndes/data/contants/imagaeasset.dart';
import 'package:test_hnndes/data/contants/styles.dart';
import 'package:test_hnndes/presentation/screen/leaves_page.dart';
import 'package:test_hnndes/presentation/widgets/custom_input_field.dart';
import 'package:test_hnndes/presentation/widgets/loading_widget_tow.dart';
import 'package:test_hnndes/presentation/widgets/title_testform.dart';

// ignore: must_be_immutable

class SigninPage extends StatefulWidget {
  const SigninPage({super.key});

  @override
  State<SigninPage> createState() => _SigninPageState();
}

class _SigninPageState extends State<SigninPage> {
  TextEditingController userName = TextEditingController();
  TextEditingController password = TextEditingController();
  bool obscureText = true;
  final SigninCubit signinBloc = SigninCubit(signinApi: SigninApi());
  @override
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => signinBloc..initial(),
      child: SafeArea(
        child: Scaffold(
            backgroundColor: AppColors.whiteColor,
            body: BlocConsumer<SigninCubit, SigninState>(
              listener: (context, state) {
                if (state is SigninSuccess) {
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text("Signin success"),
                    backgroundColor: Colors.green,
                    duration: Duration(seconds: 2),
                  ));
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (context) => const LeavesPage(),
                  ));
                } else if (state is SigninFailure) {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: const Text("Signin Failed"),
                    backgroundColor: AppColors.error,
                    duration: const Duration(seconds: 2),
                  ));
                }
              },
              builder: (context, state) {
                if (state is LoadingState) {
                  return Stack(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 32.w),
                        child: SingleChildScrollView(
                          child: GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: () =>
                                FocusManager.instance.primaryFocus!.unfocus(),
                            child: SizedBox(
                              height: ScreenUtil().screenHeight,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Center(
                                    child: SizedBox(
                                      width: 366.w,
                                      height: 200.h,
                                      child: Image.asset(
                                          AppImageAsset.welcomeimage),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 32.h,
                                  ),
                                  Center(
                                    child: Text(
                                      "Please Login To Your Account",
                                      style:
                                          MyStyle.primaryTextStyle().copyWith(
                                        fontSize: 25.sp,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 32.h,
                                  ),
                                  const CustomTitle(text: "USERNAME"),
                                  SizedBox(
                                    height: 8.h,
                                  ),
                                  CustomInputField(
                                    controller: userName,
                                    color: AppColors.whiteColor,
                                    borderRadius: 20.r,
                                  ),
                                  SizedBox(
                                    height: 32.h,
                                  ),
                                  const CustomTitle(text: "PASSWORD"),
                                  SizedBox(
                                    height: 8.h,
                                  ),
                                  CustomInputField(
                                    controller: password,
                                    color: AppColors.whiteColor,
                                    obscureText: true,
                                    borderRadius: 20.r,
                                    sufexicon: GestureDetector(
                                        child: const Icon(Icons.abc_outlined)),
                                  ),
                                  SizedBox(
                                    height: 48.h,
                                  ),
                                  Center(
                                    child: GestureDetector(
                                      child: Container(
                                        width: 400.w,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 16.w, vertical: 16.h),
                                        decoration: BoxDecoration(
                                          color: AppColors.grayColor,
                                          borderRadius:
                                              BorderRadius.circular(20.r),
                                        ),
                                        child: Center(
                                          child: Text(
                                            "Login",
                                            style: MyStyle.primaryTextStyle()
                                                .copyWith(
                                                    color:
                                                        AppColors.whiteColor),
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      LoadingWidgetTow(
                        height: 250.h,
                        width: 250.h,
                      ),
                    ],
                  );
                }
                return Padding(
                  padding: EdgeInsets.symmetric(horizontal: 32.w),
                  child: SingleChildScrollView(
                    child: GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () =>
                          FocusManager.instance.primaryFocus!.unfocus(),
                      child: SizedBox(
                        height: ScreenUtil().screenHeight,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                              child: SizedBox(
                                width: 366.w,
                                height: 200.h,
                                child: Image.asset(AppImageAsset.welcomeimage),
                              ),
                            ),
                            SizedBox(
                              height: 32.h,
                            ),
                            Center(
                              child: Text(
                                "Please Login To Your Account",
                                style: MyStyle.primaryTextStyle().copyWith(
                                  fontSize: 25.sp,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 32.h,
                            ),
                            const CustomTitle(text: "USERNAME"),
                            SizedBox(
                              height: 8.h,
                            ),
                            CustomInputField(
                              controller: userName,
                              color: AppColors.whiteColor,
                              borderRadius: 20.r,
                            ),
                            SizedBox(
                              height: 32.h,
                            ),
                            const CustomTitle(text: "PASSWORD"),
                            SizedBox(
                              height: 8.h,
                            ),
                            CustomInputField(
                              controller: password,
                              color: AppColors.whiteColor,
                              obscureText: obscureText,
                              borderRadius: 20.r,
                              sufexicon: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      obscureText = !obscureText;
                                    });
                                  },
                                  child: obscureText
                                      ? const Icon(Icons.remove_red_eye)
                                      : const Icon(
                                          Icons.remove_red_eye_outlined)),
                            ),
                            SizedBox(
                              height: 48.h,
                            ),
                            Center(
                              child: GestureDetector(
                                onTap: () => signinBloc.signin(
                                    userName.text, password.text),
                                child: Container(
                                  width: 400.w,
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 16.w, vertical: 16.h),
                                  decoration: BoxDecoration(
                                    color: AppColors.blueColor,
                                    borderRadius: BorderRadius.circular(20.r),
                                  ),
                                  child: Center(
                                    child: Text(
                                      "Login",
                                      style: MyStyle.primaryTextStyle()
                                          .copyWith(
                                              color: AppColors.whiteColor),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            )),
      ),
    );
  }
}
