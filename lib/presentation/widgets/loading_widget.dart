import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:lottie/lottie.dart';
import 'package:test_hnndes/data/contants/imagaeasset.dart';

class LoadingWidget extends StatelessWidget {
  final double? width;
  final double? height;
  const LoadingWidget({super.key, this.width, this.height});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Lottie.asset(AppImageAsset.loading,
          width: width ?? 150.h, height: height ?? 150.h),
    );
  }
}
