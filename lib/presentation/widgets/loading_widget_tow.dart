import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_hnndes/data/contants/app_colors.dart';

class LoadingWidgetTow extends StatelessWidget {
  final double? width;
  final double? height;
  const LoadingWidgetTow({super.key, this.width, this.height});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 150.w,
        width: 150.w,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.r),
          color: AppColors.whiteColor,
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(
                color: AppColors.loadingColor,
              ),
              SizedBox(
                height: 20.h,
              ),
              Text(
                "loading",
                style: TextStyle(color: AppColors.loadingColor),
              )
            ],
          ),
        ),
      ),
    );
  }
}
