import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_hnndes/data/contants/styles.dart';

class CustomTitle extends StatelessWidget {
  final String text;
  const CustomTitle({super.key, required this.text});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: MyStyle.primaryTextStyle().copyWith(
        fontWeight: FontWeight.bold,
        fontSize: 20.sp,
      ),
    );
  }
}
