import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_hnndes/data/api/leaves_api.dart';
import 'package:test_hnndes/data/model/leave_count_model.dart';
import 'package:test_hnndes/data/model/leave_model.dart';

part 'leaves_event.dart';
part 'leaves_state.dart';

class LeavesBloc extends Bloc<LeavesEvent, LeavesState> {
  final LeavesApi leavesApi;
  int pageNumber = 1;
  int totalPages = 0;
  List<LeaveModel> leaves = [];
  LeaveCount leaveCount = LeaveCount();

  LeavesBloc({required this.leavesApi}) : super(Loading()) {
    on<LeavesEvent>((event, emit) async {
      if (event is LeavesInitialEvent) {
        emit(Loading());
        var response = await leavesApi.getAllLeaves(pageNumber);
        var res = await leavesApi.getLeavesCount();
        if (response == "unAuth") {
          emit(UnAuthorizedState());
        } else if (response != "error" && res != "error") {
          for (var leave in response["data"]["leaves"]) {
            leaves.add(LeaveModel.fromJson(leave));
          }

          totalPages = response["data"]["totalPages"];
          leaveCount = LeaveCount.fromJson(res["data"]);
          emit(LeavesFirstPageLoaded());
          pageNumber++;
        } else {
          emit(LeavesFirstPageFailed());
        }
      } else if (event is LeavesFetchingEvent) {
        emit(LeavesLoading());
        var response = await leavesApi.getAllLeaves(pageNumber);
        if (response != "error") {
          for (var leave in response["data"]["leaves"]) {
            leaves.add(LeaveModel.fromJson(leave));
          }
          emit(LeavesLoaded());
          pageNumber++;
        } else {
          emit(LeavesError());
        }
      }
    });
  }
}
