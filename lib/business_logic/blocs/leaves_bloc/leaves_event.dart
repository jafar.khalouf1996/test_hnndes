part of 'leaves_bloc.dart';

@immutable
abstract class LeavesEvent {}

class LeavesInitialEvent extends LeavesEvent {}

class LeavesFetchingEvent extends LeavesEvent {}
