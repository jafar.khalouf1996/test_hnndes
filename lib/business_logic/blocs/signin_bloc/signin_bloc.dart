import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_hnndes/data/api/signin_api.dart';

part 'signin_event.dart';
part 'signin_state.dart';

class SigninBloc extends Bloc<SigninEvent, SigninState> {
  final SigninApi signinApi;
  TextEditingController userName = TextEditingController()..text = "eshemp";
  TextEditingController password = TextEditingController()..text = "Eshemp@123";
  SigninBloc({required this.signinApi}) : super(SigninInitial()) {
    on<SigninEvent>((event, emit) async {
      if (event is SigningInEvent) {
        emit(LoadingState());
        try {
          var response = await signinApi.signin(userName.text, password.text);
          if (response != "error") {
            emit(SigninSuccess());
          } else {
            emit(SigninFailure());
          }
        } catch (e) {
          emit(SigninFailure());
        }
      }
    });
  }
}
