import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_hnndes/business_logic/blocs/leaves_bloc/leaves_bloc.dart';
import 'package:test_hnndes/data/api/leaves_api.dart';
import 'package:test_hnndes/data/model/leave_count_model.dart';
import 'package:test_hnndes/data/model/leave_model.dart';

class LeavesCubit extends Cubit<LeavesState> {
  final LeavesApi leavesApi;
  int pageNumber = 1;
  int totalPages = 0;
  List<LeaveModel> leaves = [];
  LeaveCount leaveCount = LeaveCount();
  LeavesCubit({
    required this.leavesApi,
  }) : super(Loading());
  void getInitial() async {
    emit(Loading());
    var response = await leavesApi.getAllLeaves(pageNumber);
    var res = await leavesApi.getLeavesCount();
    if (response == "unAuth") {
      emit(UnAuthorizedState());
    } else if (response != "error" && res != "error") {
      for (var leave in response["data"]["leaves"]) {
        leaves.add(LeaveModel.fromJson(leave));
      }

      totalPages = response["data"]["totalPages"];
      leaveCount = LeaveCount.fromJson(res["data"]);
      emit(LeavesFirstPageLoaded());
      pageNumber++;
    } else {
      emit(LeavesFirstPageFailed());
    }
  }

  void getLeavesFetching() async {
    emit(LeavesLoading());
    var response = await leavesApi.getAllLeaves(pageNumber);
    if (response != "error") {
      for (var leave in response["data"]["leaves"]) {
        leaves.add(LeaveModel.fromJson(leave));
      }
      emit(LeavesLoaded());
      pageNumber++;
    } else {
      emit(LeavesError());
    }
  }
}
