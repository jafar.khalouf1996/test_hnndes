import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_hnndes/business_logic/blocs/signin_bloc/signin_bloc.dart';
import 'package:test_hnndes/data/api/signin_api.dart';

class SigninCubit extends Cubit<SigninState> {
  final SigninApi signinApi;

  SigninCubit({required this.signinApi}) : super(SigninInitial());

  void initial() {}
  void signin(String username, String password) async {
    {
      emit(LoadingState());
      try {
        var response = await signinApi.signin(username, password);
        if (response != "error") {
          emit(SigninSuccess());
        } else {
          emit(SigninFailure());
        }
      } catch (e) {
        emit(SigninFailure());
      }
    }
  }
}
