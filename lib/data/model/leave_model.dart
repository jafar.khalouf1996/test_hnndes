class ListModel {
  int? companyId;
  int? departmentId;
  int? employeeId;
  String? fromDate;
  String? toDate;
  List<LeaveModel>? leaves;
  int? pageNumber;
  int? totalPages;
  int? pageSize;
  int? totalSize;

  ListModel(
      {this.companyId,
      this.departmentId,
      this.employeeId,
      this.fromDate,
      this.toDate,
      this.leaves,
      this.pageNumber,
      this.totalPages,
      this.pageSize,
      this.totalSize});

  ListModel.fromJson(Map<String, dynamic> json) {
    companyId = json['companyId'];
    departmentId = json['departmentId'];
    employeeId = json['employeeId'];
    fromDate = json['fromDate'];
    toDate = json['toDate'];
    if (json['leaves'] != null) {
      leaves = <LeaveModel>[];
      json['leaves'].forEach((v) {
        leaves!.add(LeaveModel.fromJson(v));
      });
    }
    pageNumber = json['pageNumber'];
    totalPages = json['totalPages'];
    pageSize = json['pageSize'];
    totalSize = json['totalSize'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['companyId'] = companyId;
    data['departmentId'] = departmentId;
    data['employeeId'] = employeeId;
    data['fromDate'] = fromDate;
    data['toDate'] = toDate;
    if (leaves != null) {
      data['leaves'] = leaves!.map((v) => v.toJson()).toList();
    }
    data['pageNumber'] = pageNumber;
    data['totalPages'] = totalPages;
    data['pageSize'] = pageSize;
    data['totalSize'] = totalSize;
    return data;
  }
}

class LeaveModel {
  int? id;
  String? employeeName;
  int? statusId;
  String? statusName;
  String? absenceValue;
  String? number;
  int? employeeId;
  int? typeId;
  String? absenceFrom;
  String? absenceTo;
  String? notes;

  LeaveModel(
      {this.id,
      this.employeeName,
      this.statusId,
      this.statusName,
      this.absenceValue,
      this.number,
      this.employeeId,
      this.typeId,
      this.absenceFrom,
      this.absenceTo,
      this.notes});

  LeaveModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    employeeName = json['employeeName'];
    statusId = json['statusId'];
    statusName = json['statusName'];
    absenceValue = json['absenceValue'];
    number = json['number'];
    employeeId = json['employeeId'];
    typeId = json['typeId'];
    absenceFrom = json['absenceFrom'];
    absenceTo = json['absenceTo'];
    notes = json['notes'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['employeeName'] = employeeName;
    data['statusId'] = statusId;
    data['statusName'] = statusName;
    data['absenceValue'] = absenceValue;
    data['number'] = number;
    data['employeeId'] = employeeId;
    data['typeId'] = typeId;
    data['absenceFrom'] = absenceFrom;
    data['absenceTo'] = absenceTo;
    data['notes'] = notes;
    return data;
  }
}
