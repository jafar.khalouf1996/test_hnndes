class LeaveCount {
  double? maxAnnual;
  int? annual;
  int? sickness;
  int? marriage;
  int? maternity;
  int? workAccident;
  int? death;
  int? unpaid;
  int? others;
  String? hourly;

  LeaveCount(
      {this.maxAnnual,
      this.annual,
      this.sickness,
      this.marriage,
      this.maternity,
      this.workAccident,
      this.death,
      this.unpaid,
      this.others,
      this.hourly});

  LeaveCount.fromJson(Map<String, dynamic> json) {
    maxAnnual = json['maxAnnual'];
    annual = json['annual'];
    sickness = json['sickness'];
    marriage = json['marriage'];
    maternity = json['maternity'];
    workAccident = json['workAccident'];
    death = json['death'];
    unpaid = json['unpaid'];
    others = json['others'];
    hourly = json['hourly'];
  }
}
