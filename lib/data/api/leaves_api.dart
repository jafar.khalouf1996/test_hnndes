import 'package:dio/dio.dart';
import 'package:test_hnndes/data/contants/linkapi.dart';
import 'package:test_hnndes/main.dart';

class LeavesApi {
  getAllLeaves(int pageNumber) async {
    const String url = AppLink.leastApi;
    final dio = Dio();
    dio.options.headers['companyId'] = "1";
    dio.options.headers['departmentId'] = "1";
    dio.options.headers['employeeId'] = "6";
    dio.options.headers['pageNumber'] = pageNumber.toString();
    dio.options.headers['pageSize'] = "7";
    dio.options.headers["Authorization"] =
        "Bearer ${sharedPreferences!.get("access_token")}";
    try {
      var response = await dio.get(url);

      if (response.statusCode == 200) {
        return response.data;
      } else if (response.statusCode == 401) {
        return "unAuth";
      } else {
        return "error";
      }
    } catch (e) {
      return "error";
    }
  }

  getLeavesCount() async {
    String url =
        "${AppLink.countEmployee}/${sharedPreferences!.getString("emp_id")}";
    final dio = Dio();
    dio.options.headers["Authorization"] =
        "Bearer ${sharedPreferences!.get("access_token")}";
    try {
      var response = await dio.get(url);
      if (response.statusCode == 200) {
        return response.data;
      } else if (response.statusCode == 401) {
        return "unAuth";
      } else {
        return "error";
      }
    } catch (e) {
      return "error";
    }
  }
}
