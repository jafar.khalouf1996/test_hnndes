import 'package:dio/dio.dart';
import 'package:test_hnndes/data/contants/linkapi.dart';
import 'package:test_hnndes/main.dart';

class SigninApi {
  signin(String userName, String password) async {
    const String url = AppLink.login;
    final dio = Dio();
    try {
      var response = await dio
          .post(url, data: {"username": userName, "password": password});
      if (response.statusCode == 200) {
        sharedPreferences!.setString(
            "access_token", response.data["data"]["accessToken"].toString());
        sharedPreferences!.setString("emp_id",
            response.data["data"]["user"]["employee"]["id"].toString());
        sharedPreferences!.setString("emp_name",
            response.data["data"]["user"]["employee"]["fullName"].toString());

        return response.data;
      } else {
        return "error";
      }
    } catch (e) {
      rethrow;
    }
  }
}
