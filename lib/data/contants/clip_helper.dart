import 'package:flutter/material.dart';

class MyClip extends CustomClipper<Path> {
  @override
  // Path getClip(Size size) {
  //   var path = Path();
  //   path.lineTo(0, size.height * 0.5);
  //   var controlPoint1 = Offset(size.width * 0.25, size.height);
  //   var endpoint1 = Offset(size.width * 0.5, size.height * 0.6);
  //   path.quadraticBezierTo(
  //       controlPoint1.dx, controlPoint1.dy, endpoint1.dx, endpoint1.dy);
  //   var controlPoint2 = Offset(size.width * 0.75, size.height * 0.2);
  //   var endpoint2 = Offset(size.width, size.height * 0.8);
  //   path.quadraticBezierTo(
  //       controlPoint2.dx, controlPoint2.dy, endpoint2.dx, endpoint2.dy);
  //   path.lineTo(size.width, 0);
  //   path.close();
  //   return path;
  // }
  Path getClip(Size size) {
    Path path = Path();
    path.moveTo(0, size.height * 0.3835286);
    path.lineTo(0, 0);
    path.lineTo(size.width, 0);
    path.quadraticBezierTo(size.width, size.height * 0.2563179, size.width,
        size.height * 0.3417571);
    path.cubicTo(
        size.width * 0.8302917,
        size.height * 0.3682286,
        size.width * 0.5813083,
        size.height * 0.8092714,
        size.width * 0.2901500,
        size.height * 0.7983286);
    path.quadraticBezierTo(size.width * 0.1313500, size.height * 0.7725571, 0,
        size.height * 0.3835286);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
