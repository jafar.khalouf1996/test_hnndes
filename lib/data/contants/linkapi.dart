class AppLink {
  static const String server = "https://staging.api.hr-portals.com/api/v1";

//api
  static const String leastApi = "$server/Leave/List";
  static const String countEmployee = "$server/Employee/LeaveCount";

// ================================= Auth ========================== //
  static const String login = "$server/Auth/login";
}
