class AppImageAsset {
  //root
  static const String rootImages = "assets/images";
  static const String rootLottie = "assets/lottie";
  //
//images
  static const String icondata = "$rootImages/date.png";
  static const String iconnote = "$rootImages/note.png";
  static const String iconstatus = "$rootImages/status.png";
  static const String welcomeimage = "$rootImages/Welcome.png";
//lottie
  static const String loading = "$rootLottie/loading.json";
}
