import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_hnndes/data/contants/app_colors.dart';

class MyStyle {
  static TextStyle primaryTextStyle() => TextStyle(
        fontSize: 20.sp,
        color: AppColors.blackColor,
      );

  static TextStyle secondaryTextStyle() => TextStyle(
        fontSize: 24.sp,
        color: AppColors.primaryTextColor,
      );
}
